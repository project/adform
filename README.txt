Adform integration module for Drupal
====================================

Provides basic page tracking and API for tracking product views.

This module was created for a specific project, so it might not be
plug and play in all cases yet.

Module is developed at Reload! (http://reload.dk/) and sponsored by
Stofa (http://stofa.dk/).
